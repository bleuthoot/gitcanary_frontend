import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import RepoOverview from "@/views/RepoOverview";
import Login from '../views/Login.vue'
import store from '../store' // your vuex store
Vue.use(VueRouter)

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters["auth/isAuthenticated"]) {
        next()
        return
    }
    next('/')
}

const ifAuthenticated = (to, from, next) => {
    if (store.getters["auth/isAuthenticated"]) {
        next()
        return
    }
    next('/login')
}

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            title: 'GitCanary'
        },
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        beforeEnter: ifNotAuthenticated,
    },
    {
        path: '/repo/:repoId',
        component: RepoOverview,
        name: 'Repo',
        beforeEnter: ifAuthenticated
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
