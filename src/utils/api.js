import axios from "axios";

let url = 'https://gitcanary-api.hboictlab.nl';

if(document.documentURI.includes('localhost')) {
    url = 'http://localhost:8000/';
}

const api = axios.create({
      baseURL: url,
        headers: {
            'Content-Type': 'application/json',
        }

    })

export default api;