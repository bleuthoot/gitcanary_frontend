export const chartColors = [
    'rgba(255, 159, 64, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(178,34,34, 1)',
    'rgba(85,107,47, 1)',
    'rgba(138,43,226, 1)',
    'rgba(139,69,19, 1)',
    'rgba(188,143,143, 1)',
    'rgba(112,128,144, 1)'
];


export default chartColors;