import api from "@/utils/api";
import Vue from "vue";

const state = {
    reports: {},
    dateMin: null,
    dateMax: null,
    startDate: null,
    endDate: Date.now(),
    branches: ['all'],
    currentBranch: 'all',
    loading: false,
    foundCommitters: []
};

const getters = {
    getReports: state => {
        return state.reports
    },
    getReport: (state) => (type) => {
        return state.reports[type]
    },
    getBranches: (state) => {
        return state.branches
    },
    getCurrentBranch: (state) => {
        return state.currentBranch
    },
    getEndDate: (state) => {
        return state.endDate
    },
    getStartDate: (state) => {
        return state.startDate
    },
    getDateMin: (state) => {
        return state.dateMin;
    },
    getDateMax: (state) => {
        return state.dateMax;
    },
    getFilteredReport: (state) => (type)  => {
        let commits = [];
        if(Object.keys(state.reports).length > 0) {
            state.reports[type].forEach((item) => {
                if (withinTimeRange(state, item.date) && checkBranch(state, item.branches)) {
                    commits.push(item)
                }
            });
        }
        return commits;
    },
    foundCommitters: (state) => {
        return state.foundCommitters;
    }
};


const actions = {
    fetchReports: ({state, commit}, repo) => {
        api.get("repo/" + repo.id + "/reports/")
            .then(resp => {
                commit('resetDates')
                let data = resp.data
                let reports = {}
                data.forEach((report) => {
                    if (report.type == 'base') {
                        reports[report.type] = processBaseReport(report.json, repo.committers, commit)
                    }
                    if (report.type == 'gitblame') {
                        reports[report.type] = aggregateGitBlame(report.json, repo.committers)
                    }
                })
                commit('updateReports', reports);
                commit('updateStartDate', state.dateMin);
                commit('updateEndDate', state.dateMax);
                commit('stopLoading');
            })
            .catch((resp) => {
                console.log(resp)

            });
    },
    emptyReports: ({commit}) => {
        commit('emptyReports');
    }
};

function processBaseReport(jsondata, contributors, commit) {
    let data = jsondata['commits'];
    commit('emptyBranches');
    //let unknownCommitter = '';
    data.forEach((item) => {
        commit('addKnownCommitter', item.author);
        for (let contributor in contributors) {
            if (contributors[contributor].indexOf(item.author.normalize("NFD").replace(/[\u0300-\u036f]/g, "")) > -1) {
                item.author = contributor;
            }
        }

        item.branches.forEach((branch) => {
            commit('addBranch', branch);
        })
        commit('addDate', item.date)
    });

    return data;
}

function aggregateGitBlame(gitblame, contributors) {
    let newGitBlame = {};
    for (let type in gitblame) {
        newGitBlame[type] = {}
        for (let contrib in contributors) {
            newGitBlame[type][contrib] = 0
            for (let committer in gitblame[type]) {
                if (contributors[contrib].indexOf(committer.normalize("NFD").replace(/[\u0300-\u036f]/g, "")) > -1) {
                    newGitBlame[type][contrib] += gitblame[type][committer];
                }
            }
        }


    }
    return newGitBlame
}

function withinTimeRange (state, date) {
    let withinRange = true;
    date = new Date(date);
    if (state.startDate) {
        if (date <= state.startDate) {
            withinRange = false;
        }
    }
    if (state.endDate) {
        if (date >= state.endDate) {
            withinRange = false;
        }
    }
    return withinRange;
}
function checkBranch (state, branches) {
    if (state.currentBranch == 'all') {
        return true;
    }
    if (branches.indexOf(state.currentBranch) > -1) {
        return true;
    }
    return false;
}

const mutations = {
    addDate: (state, date) => {
        date = new Date(date);
        if (state.dateMin == null || date < state.dateMin) {
            state.dateMin = date;
        }
        /*
        if (state.dateMax == null || date > state.dateMax) {
            state.dateMax = date;
        }

         */
    },
    updateStartDate: (state, date) => {
        state.startDate = date;
    },
    updateEndDate: (state, date) => {
        state.endDate = date;
    },
    resetDates: (state) => {
        state.dateMin = null;
        state.dateMax = null;
    },
    addBranch: (state, branch) => {
        if (state.branches.indexOf(branch) == -1) {
            state.branches.push(branch);
        }
    },
    emptyBranches: (state) => {
        state.branches = ['all'];
    },
    addKnownCommitter: (state, committer) => {
        if(state.foundCommitters.indexOf(committer) == -1) {
            state.foundCommitters.push(committer)
        }
    },
    updateReports: (state, data) => {
        Vue.set(state, 'reports', {...data})
    },
    emptyReports: (state) => {
        state.reports = [];
        state.foundCommitters = [];
        state.branches = ['all'];
    },
    updateCurrentBranch: (state, branch) => {
        state.currentBranch = branch;
    },
    startLoading: (state) => {
        state.loading = true
    },
    stopLoading: (state) => {
        state.loading = false
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};