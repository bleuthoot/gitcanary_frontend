import api from "@/utils/api";
import Vue from "vue";

const state = {
    repos: [],
    currentRepo: null
};

const getters = {
    getRepos: state => state.repos,
    getRepo: (state) => (id) => {
        return state.repos.find(repo => repo.id == id)
    },
    getCurrentRepo: (state) => state.currentRepo,

};


const actions = {
    fetchRepos: ({commit, dispatch}, repoId = null) => {
        api.get("repo/")
            .then(resp => {
                commit('updateRepos', resp);
                if (repoId) {
                    dispatch('setCurrentRepo', repoId);
                }
            })
            .catch((resp) => {
                console.log(resp)

            });
    },
    setCurrentRepo: ({commit, dispatch}, repoId) => {
        if(state.repos.length > 0) {
            let repo = state.repos.find(repo => repo.id == repoId)
            dispatch("reports/fetchReports", repo, {root: true})
            commit('updateCurrentRepo', repo)
        }
    },
    emptyRepos: ({commit}) => {
        commit('emptyRepos');
    },


};

const mutations = {
    updateCurrentRepo: (state, repo) => {
        Vue.set(state, 'currentRepo', repo)
    },
    updateCommitters: (state, committers) => {
        state.currentRepo.committers = {...committers};
        api.patch("repo/" + state.currentRepo.id + "/", {committers: committers});
    },
    emptyCurrentRepo: (state) => {
        state.currentRepo = null
    },
    updateRepos: (state, resp) => {
        Vue.set(state, 'repos', [...resp.data])
    },
    emptyRepos: (state) => {
        state.repos = [];
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};