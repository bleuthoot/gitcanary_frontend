import {
    AUTH_REQUEST,
    AUTH_ERROR,
    AUTH_SUCCESS,
    AUTH_LOGOUT
} from "../actions/auth";
//import axios from 'axios'
// import {USER_REQUEST} from "@/store/actions/user";
//import {SERVER} from "@/store/actions/server";
import api from "@/utils/api";

const state = {
    token: localStorage.getItem("user-token") || "",
    username: localStorage.getItem("user-name") || "",
    status: "",
    hasLoadedOnce: false
};

const getters = {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
    userName: state => state.username
}

const actions = {
    [AUTH_REQUEST]: ({commit, dispatch}, user) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_REQUEST);
            api.post('api-token-auth/', {username: user.username, password: user.password})
                .then(resp => {
                    localStorage.setItem("user-token", resp.data.token);
                    localStorage.setItem("user-name", user.username);
                    // Here set the header of your ajax library to the token value.
                    // example with axios

                    api.defaults.headers['Authorization'] = "Token " + resp.data.token;
                    commit(AUTH_SUCCESS, resp);
                    commit('auth/updateUserName', user.username, {root: true});
                    dispatch('repos/fetchRepos', null, {root: true});
                    resolve(resp);
                })
                .catch(err => {
                    commit(AUTH_ERROR, err);
                    localStorage.removeItem("user-token");
                    localStorage.removeItem('user-name');
                    reject(err);
                });
        });
    },
    [AUTH_LOGOUT]: ({commit, dispatch}) => {
        return new Promise(resolve => {
            commit(AUTH_LOGOUT);
            localStorage.removeItem("user-token");
            localStorage.removeItem('user-name');
            dispatch('repos/emptyRepos', null, {root: true});
            dispatch('reports/emptyReports', null, {root: true})
            resolve();
        });
    }
};


// basic mutations, showing loading, success, error to reflect the api call status and the token when loaded

const mutations = {
    updateUserName: (state, username) => {
        state.username = username
    },
    [AUTH_REQUEST]: state => {
        state.status = "loading";
    },
    [AUTH_SUCCESS]: (state, resp) => {
        state.status = "success";
        state.token = resp.data.token;
        state.hasLoadedOnce = true;
    },
    [AUTH_ERROR]: state => {
        state.status = "error";
        state.hasLoadedOnce = true;
    },
    [AUTH_LOGOUT]: state => {
        state.token = "";
    }
}


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}